package config

import (
	"cars/model"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func DbConnect() *gorm.DB {
	consStr := "root:password@tcp(127.0.0.1:3306)/db_cars?parseTime=true"
	db, err := gorm.Open("mysql", consStr)
	if err != nil {
		log.Fatal("Error when connect db" + consStr + " : " + err.Error())
		return nil
	}

	db.Debug().AutoMigrate(
		model.Car{},
	)
	return db
}
