package main

import (
	carHandler "cars/car/handler"
	carRepo "cars/car/repo"
	carUseCase "cars/car/useCase"
	"cars/config"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	port := "8080"

	db := config.DbConnect()
	defer db.Close()

	router := gin.Default()

	carRepo := carRepo.CreateCarRepo(db)
	carUseCase := carUseCase.CreateCarUseCase(carRepo)
	carHandler.CreateCarHandler(router, carUseCase)

	err := router.Run(":" + port)
	if err != nil {
		log.Fatal(err)
	}
}
