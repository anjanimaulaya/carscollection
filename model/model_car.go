package model

type Car struct {
	Id           int    `json:"id"`
	Name         string `json:"name"`
	CarModel     string `json:"car_model"`
	CarColor     string `json:"car_color"`
	CarModelYear int    `json:"car_model_year"`
	Carvin       string `json:"car_vin"`
	Price        string `json:"price"`
	Availability bool   `json:"availability"`
}

func (e *Car) TableName() string {
	return "car"
}
