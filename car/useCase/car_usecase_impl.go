package useCase

import (
	"cars/car"
	"cars/model"
)

type CarUseCaseImpl struct {
	carRepo car.CarRepo
}

func CreateCarUseCase(carRepo car.CarRepo) car.CarUseCase {
	return &CarUseCaseImpl{carRepo}
}

func (e *CarUseCaseImpl) Create(car *model.Car) (*model.Car, error) {
	return e.carRepo.Create(car)
}

func (e *CarUseCaseImpl) FindAll() (*[]model.Car, error) {
	return e.carRepo.FindAll()
}

func (e *CarUseCaseImpl) FindById(id int) (*model.Car, error) {
	return e.carRepo.FindById(id)
}

func (e *CarUseCaseImpl) Update(id int, car *model.Car) (*model.Car, error) {
	return e.carRepo.Update(id, car)
}

func (e *CarUseCaseImpl) Delete(id int) error {
	return e.carRepo.Delete(id)
}
