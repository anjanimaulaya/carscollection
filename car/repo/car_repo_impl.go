package repo

import (
	"cars/car"
	"cars/model"
	"fmt"

	"github.com/jinzhu/gorm"
)

type CarRepoImpl struct {
	DB *gorm.DB
}

func CreateCarRepo(DB *gorm.DB) car.CarRepo {
	return &CarRepoImpl{DB}
}

func (e *CarRepoImpl) Create(car *model.Car) (*model.Car, error) {
	err := e.DB.Save(&car).Error
	if err != nil {
		fmt.Printf("[CarRepoImpl.Create] error execute query %v \n", err)
		return nil, fmt.Errorf("Failed to insert data")
	}
	return car, nil
}

func (e *CarRepoImpl) FindAll() (*[]model.Car, error) {
	var cars []model.Car
	err := e.DB.Find(&cars).Error
	if err != nil {
		fmt.Printf("[CarRepoImpl.FindAll] error execute query %v \n", err)
		return nil, fmt.Errorf("Failed to show all data")
	}
	return &cars, nil
}

func (e *CarRepoImpl) FindById(id int) (*model.Car, error) {
	var car = model.Car{}
	err := e.DB.Table("car").Where("id = ?", id).First(&car).Error
	if err != nil {
		fmt.Printf("[CarRepoImpl.FindById] error execute query %v \n", err)
		return nil, fmt.Errorf("Id is not exist")
	}
	return &car, nil
}

func (e *CarRepoImpl) Update(id int, car *model.Car) (*model.Car, error) {
	var newCar = model.Car{}
	err := e.DB.Table("car").Where("id = ?", id).First(&newCar).Update(&car).Error
	if err != nil {
		fmt.Printf("[CarRepoImpl.Update] error execute query %v \n", err)
		return nil, fmt.Errorf("Failed to update data")
	}
	return &newCar, nil
}

func (e *CarRepoImpl) Delete(id int) error {
	var car = model.Car{}
	err := e.DB.Table("car").Where("id = ?", id).First(&car).Delete(&car).Error
	if err != nil {
		fmt.Printf("[CarRepoImpl.Delete] error execute query %v \n", err)
		return fmt.Errorf("Id is not exist")
	}
	return nil
}
