package car

import "cars/model"

type CarUseCase interface {
	Create(car *model.Car) (*model.Car, error)
	FindAll() (*[]model.Car, error)
	FindById(id int) (*model.Car, error)
	Update(id int, car *model.Car) (*model.Car, error)
	Delete(id int) error
}
