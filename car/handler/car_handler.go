package handler

import (
	"cars/car"
	"cars/model"
	"cars/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CarHandler struct {
	carUseCase car.CarUseCase
}

func CreateCarHandler(r *gin.Engine, carUseCase car.CarUseCase) {
	carHandler := CarHandler{carUseCase}

	r.POST("/car", carHandler.addCar)
	r.GET("/car", carHandler.GetAllCars)
	r.GET("car/:id", carHandler.GetCarById)
	r.PUT("/car/:id", carHandler.editCar)
	r.DELETE("/car/:id", carHandler.deleteCar)
}

func (e *CarHandler) addCar(c *gin.Context) {
	var car = model.Car{}
	err := c.Bind(&car)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Internal server error...")
		return
	}
	if car.Id != 0 {
		utils.HandleError(c, http.StatusBadRequest, "Input id is not permitted")
		return
	}
	if car.Name == "" || car.CarModel == "" || car.CarColor == "" || car.CarModelYear == 0 || car.Carvin == "" || car.Price == "" {
		utils.HandleError(c, http.StatusBadRequest, "column can't be null")
		return
	}
	newCar, err := e.carUseCase.Create(&car)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		return
	}
	utils.HandleSuccess(c, newCar)
}

func (e *CarHandler) GetAllCars(c *gin.Context) {
	cars, err := e.carUseCase.FindAll()
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		return
	}
	if len(*cars) == 0 {
		utils.HandleError(c, http.StatusNotFound, "List cars is empty")
	}
	utils.HandleSuccess(c, cars)
}

func (e *CarHandler) GetCarById(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "id has be number")
		return
	}
	car, err := e.carUseCase.FindById(id)
	if err != nil {
		utils.HandleError(c, http.StatusNotFound, err.Error())
		return
	}
	utils.HandleSuccess(c, car)
}

func (e *CarHandler) editCar(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "id has be number")
		return
	}
	_, err = e.carUseCase.FindById(id)
	if err != nil {
		utils.HandleError(c, http.StatusNotFound, err.Error())
		return
	}
	var tempCar = model.Car{}
	err = c.Bind(&tempCar)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Internal server error...")
		return
	}
	if tempCar.Id != 0 {
		utils.HandleError(c, http.StatusBadRequest, "input is not permitted")
		return
	}
	if tempCar.Name == "" || tempCar.CarModel == "" || tempCar.CarColor == "" || tempCar.CarModelYear == 0 || tempCar.Carvin == "" || tempCar.Price == "" {
		utils.HandleError(c, http.StatusBadRequest, "column can't be null")
		return
	}
	car, err := e.carUseCase.Update(id, &tempCar)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		return
	}
	utils.HandleSuccess(c, car)
}

func (e *CarHandler) deleteCar(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "id has be number")
		return
	}
	err = e.carUseCase.Delete(id)
	if err != nil {
		utils.HandleError(c, http.StatusNotFound, err.Error())
		return
	}
	utils.HandleSuccess(c, "success delete data")
}
